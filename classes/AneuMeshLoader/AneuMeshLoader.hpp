//
// Created by Михаил Кочетков on 24/10/2018.
//

#ifndef LAB1_ANEUMESHLOADER_HPP
#define LAB1_ANEUMESHLOADER_HPP

#pragma once

#include "../MeshLoader/MeshLoader.hpp"

#include <string>
#include <vector>
#include <ostream>

class AneuMeshLoader : public MeshLoader {
public:
    void LoadMesh(std::string filePath) override;
};

std::vector<std::string> splitString(std::string str, std::string del);
void ltrim(std::string &str);
void uniqueSpaces(std::string &str);

#endif //LAB1_ANEUMESHLOADER_HPP
